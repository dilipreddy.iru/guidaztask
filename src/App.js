import React from 'react';
import Board from './components/Board';
import Card from './components/Card';
import Chart from './components/Chart';
import JointChart from './components/JointChart';

function App() {
  return (
    <div className="App">
     <main className="flexbox">
       
       <Board id="board-1" className="board">
         <Card id="card-1" className="card" draggable="true">
          <Chart />
          
         </Card>
         <Card id="card-2" className="card" draggable="true">
         <JointChart />
         </Card>
         
       </Board>

       <Board id="board-2" className="boardTwo">
        
       </Board>
     </main>
    </div>
  );
}

export default App;
